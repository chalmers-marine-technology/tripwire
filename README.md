# tripWíre

A numerical tripwire based on introducing a random force into the momemntum equation in cells witing a given cellzone.
Implemented as a `fvOption`.

Compiles for the Foundation version 4.1.
Does not compile for v1806 as is.
Change `vector01()` to `sample01<vector>()` tripWireTemplates.C  to fix.

## Algorithm

Three input parameters are required

- `Uref`, $`U_{ref}`$, a reference velocity scale.
- `turbulenceIntensity`, $`I_{ref}`$, a refence turbulence intensity.
- `relaxationFactor`, $`\alpha`$, a relaxation factor for thechange in the introduced random values.

Based on the above the following source term $`S`$ applied at each cell in the zone

```math
\begin{aligned}
& s^n = (1 - \alpha)s^{n-1} + \alpha \frac{U_{ref} I_{ref}}{\Delta t} \cdot RNG[-0.5, 0.5]\\
& S^n = V_{cell}s^n
\end{aligned}
```
see the `addRandomIntertialTerm` function.
Here $`RNG[]`$ refers to a random number in the given interval.

## Sample fvOptions input

See also the fvOptions file.

````
myTrip
{
    type        tripWire;

    tripWireCoeffs
    {
        selectionMode cellZone;
        cellZone    tripSet;

        active      true;

        fields (U);
        fieldNames  (U);

        Uref    Uref [0 1 -1 0 0 0 0] 20.4;
        turbulenceIntensity turbulenceIntensity [0 0 0 0 0 0 0] 0.05;
        relaxationFactor 1.0;
    }
}
````
